import React from "react";
import LoadingApp from "./containers/useRef/LoadingApp";
import FocusApp from "./containers/useRef/FocusApp";
import CopyApp from "./containers/useRef/CopyApp";
import MediaPlayer from "./containers/useRef/MediaPlayer";
import ScrollAnimation from "./containers/useRef/ScrollAnimation";
import RenderApp from "./containers/useRef/RenderApp";

function App() {
  return (
    <div className="App">
      <LoadingApp />
      <MediaPlayer />
      <FocusApp />
      <CopyApp />
      <RenderApp />
      <ScrollAnimation /> 
      {/* 
      */}
    </div>
  );
}

export default App;
