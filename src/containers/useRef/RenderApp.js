import React, { useEffect, useRef, useState } from 'react'

const RenderApp = () => {
    const [text, setText] = useState("");
    const renderRef = useRef(0);

    // setRenders(renders+1);
    useEffect(() => {
        // const render = renderRef.current;
        // render++;
        renderRef.current++;
        console.log('render');
    });

    return (
        <div>
            <h1>App Render</h1>
            <input type="text" value={text}  onChange={(e) => setText(e.target.value)}/>
            <h1>Renders: {renderRef.current}</h1>
        </div>
    )
}

export default RenderApp
