import React, { useState, useRef } from "react";

function MediaPlayer() {
    const [isPlaying, setIsPlaying] = useState(false)
    const videoRef = useRef();

    const halseToggle = () => {
        const video = videoRef.current;
        console.log(videoRef);
        console.log(videoRef.current);
        isPlaying ? video.pause() : video.play();
        setIsPlaying(!isPlaying);
    }

    return <div>
        <video width="400" ref={videoRef}>
            <source src="video/sheep.mp4" type="video/mp4"/>
        </video>
        <button onClick={halseToggle}> {isPlaying ? 'Pause' : 'Play'}</button>

    </div>
}

export default MediaPlayer;