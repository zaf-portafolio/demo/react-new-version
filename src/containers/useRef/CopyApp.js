import React, { useState, useRef } from "react";

function CopyApp() {
    const [text, setText] = useState("Hello");
    const [isCopied, setIsCopied] = useState(false)
    const inputRef = useRef();

    const handleCopy = () => {
        console.log(inputRef);
        console.log(inputRef.current);
        const input = inputRef.current;
        input.select();
        document.execCommand('copy');

        setIsCopied(true);
        setTimeout(() => {
            setIsCopied(false);
        }, 2000);
    }



    return <div>
        <input type="text"
            ref={inputRef}
            value={text}
            onChange={(e) => setText(e.target.value)} />
        <button onClick={handleCopy}> Copy App </button>
        {isCopied && <h1>Copado</h1>}
    </div>
}

export default CopyApp;