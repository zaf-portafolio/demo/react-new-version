import React, { useState, useRef } from "react";

function App() {
    const [text, setText] = useState("Hello");
    const inputRef = useRef();

    const handleFocus = () => {
        const input = inputRef.current;
        console.log(inputRef);
        console.log(inputRef.current);
        input.focus();
    }



    return <div>
        <input type="text"
            ref={inputRef}
            value={text}
            onChange={(e) => setText(e.target.value)} />
        <button onClick={handleFocus}> Focus</button>

    </div>
}

export default App;