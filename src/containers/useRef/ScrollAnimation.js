import React, { useState, useRef, useEffect } from "react";

function ScrollAnimation() {
    const [background, setBackground] = useState("pink");
    const divRef = useRef();

    useEffect(() => {
        const handleScroll = () => {
            const div = divRef.current;
            const  { y } = div.getBoundingClientRect();
            const backgroundColor = y > 0 ? 'orange' : 'red';
            console.log(y);
            setBackground(backgroundColor);
            
        }
        window.addEventListener('scroll', handleScroll);

        return ()  => {
            window.removeEventListener('scroll', handleScroll);
        }

    }, [])

    return <div>
        <div style={{ height: "180vh", backgroundColor: background }} ref={divRef}>
            <h1>Scroll to change back</h1>
        </div>
    </div>
}

export default ScrollAnimation;