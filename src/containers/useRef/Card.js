import React, { useEffect, useRef, useState } from 'react'
import getPost from "../../helpers/api";

const Card = () => {
    const [post, setPost] = useState({ title: "post1" });
    const [loading, setLoading] = useState(true);
    const isMountedRef = useRef(true);

    const updatePost = () => getPost().then((newPost) => {
        setTimeout(() => {
            if (isMountedRef.current) {
                setPost(newPost);
                setLoading(false);
            }
        }, 2000);
    });

    useEffect(() => {
        updatePost();
        return () => {
            isMountedRef.current = false;
        }
    }, []);

    return (
        <div>
            <h1>{post.title}</h1>
            {loading && <h1>LOADING...</h1>}
        </div>
    )
}

export default Card
