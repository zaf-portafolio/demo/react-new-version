const getPost = async() => {
    const url = "https://jsonplaceholder.typicode.com/posts/1";
    const resp = await fetch(url);
    const data = await resp.json();
    return data;
}
export default getPost;